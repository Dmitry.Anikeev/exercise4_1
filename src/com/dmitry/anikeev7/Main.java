package com.dmitry.anikeev7;

import com.dmitry.anikeev7.Class04;
/**
 * @author Anikeev Dmitry
 * @Email: dmitry.anikeev7@@gmail.com
**/
public class Main {
    final static int SIZEARRAY = 20;

    public static void main(String[] args) {
	int[] arr = new int[SIZEARRAY];

	for (int i = 0; i < SIZEARRAY; i++) {
	    arr[i] = (int) (20 * Math.random() - 10);
	}
	// The array if ready
	System.out.println("Original array:");

	for (int i = 0; i < SIZEARRAY; i++) {
	    System.out.print(arr[i] + "  ");
	}
	Class04.func01(arr, SIZEARRAY); // completing task 1

	Class04.func02(arr, SIZEARRAY); // completing task 2

	Class04.func03(arr, SIZEARRAY); // completing task 3

	Class04.func04(arr, SIZEARRAY); // completing task 4

	Class04.func05(arr, SIZEARRAY); // completing task 5

	Class04.func06(arr, SIZEARRAY); // completing task 6
    }
}
