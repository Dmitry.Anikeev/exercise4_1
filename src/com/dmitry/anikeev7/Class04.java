package com.dmitry.anikeev7;

public class Class04 {

    static void func01(int[] arr, int SIZEARRAY) {
	int[] arr1 = arr.clone();
	int buf_p = 11;
	int index_p = 0;
	int buf_o = -11;
	int index_o = 0;

	for (int i = 0; i < SIZEARRAY; i++) {
	    if (arr1[i] < 0) { // finding the largest negative number
		if (arr1[i] > buf_o) {
		    buf_o = arr1[i];
		    index_o = i;
		}
	    } else if ((arr1[i] < buf_p) && (arr1[i] != 0)) { // finding the largest positive number
		buf_p = arr1[i];
		index_p = i;
	    }
	}
	arr1[index_p] = buf_o;
	arr1[index_o] = buf_p;
	System.out.println();
	System.out.println("1) EXchage max negative and min positive elements:");
	for (int i = 0; i < SIZEARRAY; i++) {
	    System.out.print(arr1[i] + "  ");
	}
    }

    static void func02(int[] arr, int SIZE_ARRAY) {
	System.out.println();
	System.out.println("2) Sum of elements on even positions:");
	int sum = 0;
	for (int i = 0; i < SIZE_ARRAY; i += 2) {
	    sum += arr[i];
	}
	System.out.println(sum);
    }

    static void func03(int[] arr, int SIZE_ARRAY) {
	System.out.println("3)Replace negative values with 0:");
	int[] arr3 = arr.clone();
	for (int i = 0; i < SIZE_ARRAY; i++) {
	    if (arr3[i] < 0)
		arr3[i] = 0;
	    System.out.print(arr3[i] + "  ");
	}
    }

    static void func04(int[] arr, int SIZE_ARRAY) {
	int[] arr4 = arr.clone();
	System.out.println();
	System.out.println("4)Multiply by 3 each positive element standing before negative one:");
	for (int i = 0; i < SIZE_ARRAY - 1; i++) {
	    if ((arr4[i + 1] < 0) && (arr4[i] > 0))
		arr4[i] *= 3;
	}
	for (int i = 0; i < SIZE_ARRAY - 1; i++) {
	    System.out.print(arr4[i] + "  ");
	}
    }

    static void func05(int[] arr, int SIZE_ARRAY) {
	System.out.println();
	System.out.println("5) Difference between average and min element in array:");
	int summ = 0;
	int min = 11;
	for (int i = 0; i < SIZE_ARRAY; i++) {
	    summ += arr[i];
	    if (min > arr[i])
		min = arr[i];
	}

	System.out.println((double) summ / SIZE_ARRAY - min);
    }

    static void func06(int[] arr, int SIZE_ARRAY) {
	boolean com = false; // checking for repeatability
	System.out.println("6)Elements which present more than one time and stay on odd index:");
	for (int k = -10; k <= 10; k++) {
	    for (int i = 1; i < SIZE_ARRAY; i += 2) {
		if (com) {
		    com = false;
		    break;
		}
		for (int j = 0; j < SIZE_ARRAY; j++) {

		    if (arr[i] == arr[j] && i != j) {
			if (arr[i] == k) {
			    System.out.print(arr[i] + "  ");
			    com = true;
			    break;
			}
		    }
		}
	    }
	}
    }
}
